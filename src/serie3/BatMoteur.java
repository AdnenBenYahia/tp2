package serie3;

public class BatMoteur extends Bateau{

	TypeBateau type;
	
	public BatMoteur(String nom, int poids, Capitaine capitaine){
		super(nom,poids,capitaine);
		type = TypeBateau.BteauAMoteur;
	}
	
	public String getTypeBateau()
	{
		return type.name();
	}
}

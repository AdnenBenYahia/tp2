package serie3;

public class Bateau 
{
	private String nom;
	private int tonnage;
	public  EquipageCommande equipage;
	public enum TypeBateau {
		BateauARames,BateurAvoiles,BteauAMoteur
	}
	
	// on met en abstract
    //public abstract String getTypeBateau();	

	public Bateau(String nom, int tonnage, Capitaine capitaine){
		this.nom = nom;
		this.tonnage = tonnage;
		this.equipage = new EquipageCommande(capitaine);
	}
	public String getNom() 
	{
		return nom;
	}

	public int getTonnage() 
	{
		return tonnage;
	}

	public EquipageCommande getEquipage() 
	{
		return equipage;
	}
	public void setEquipage(EquipageCommande equipage) 
	{
		this.equipage = equipage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bateau other = (Bateau) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bateau [nom=" + nom + 
				", tonnage=" + tonnage +
				", nom du capitaine= "+ equipage.getCapitaine().getNom() + 
				" "+equipage.getCapitaine().getPrenom()+
				", "+ equipage + "]";
	}
}

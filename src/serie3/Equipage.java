
package serie3;

import java.util.Arrays;

import serie3.Marin;

public class Equipage 
{
	   Marin [] marins;
	   int n=5;
	   public Equipage(int n)
	   {
	    marins = new Marin[n];//tableau de marins de taille n
	   }
	  //la m�thode toString()
	    @Override
	   public String toString() 
	   {
	    return "Equipage [marins=" + Arrays.toString(marins) + ", n=" + n + "]";
	   }
	   //le nombre maximal de marins
	   public int getCapacite()
	   {
	     int i=0;
	     while( i<=n-1 && this.marins[i] != null)
	     {
	      i++;
	     }
	     return i;
	   }
	  //Afficher le nombre de marins pr�sents dans l'�quipage
	   public int getNombreMarins()
	   {
	    int i =0, nbre =0;
	    while(i<n)
	    {
	     if(this.marins[i]!=null)
	     {
	      nbre = nbre+1;
	     }
	     i++;
	    }
	    return nbre;		
	   }
	  //la m�thode isMarinPresent
	   public boolean isMarinPresent(Marin mcheck)
	   {
	    for(int i=0;i<marins.length;i++)
	    {
	     if ((this.marins[i] != null) && (this.marins[i].equals(mcheck)))
	     {
	       return true;
	     }
	    }
	    return false;
	   }
	  //ajouter un marin � l'�quipage
	   public boolean addMarin(Marin m)
	   {
	    if(this.isMarinPresent(m) == false)
	    {
	     int indice = getCapacite();
	     if(indice == n)
	     {
	      return false;
	     }
	     else
	     {
	      this.marins[indice] = m;
	      this.etendEquipage(this.n/2);
	      return true;
	     }
	    }
	    else
	    {
	     return false;
	    }
	   }

	   
	  //Retirer l'equipage
	   public boolean removeMarin(Marin m)
	   {
	    for(int i=0;i<n;i++)
	    {
	     if(this.marins[i]==m)
	     {
	      this.marins[i] = null;
	      return true;
	     }
	    }			
	    return false;
	   }

	   public void clear()
	   {
	     for(int i=0;i<n;i++)
	     {
	      this.marins[i]=null;
	     }
	     System.out.println(this);
	   }
     /*Ajouter � l'�quipage les marins d'un �quipage pass� en param�tre*/
	   public void addAllEquipage(Equipage equipe)
	   {
	    boolean ok =true;
	    int nbMarin= equipe.getNombreMarins();
	    
	    int nbre=0;
	    Equipage equipe1 = new Equipage (this.n);
	    for(int k=0;k<this.n;k++)
	    {
	     equipe1.marins[k] = this.marins[k]; 
	    }

	    for(int j=0;j<equipe.n;j++)
	    {
	     if(!equipe1.isMarinPresent(equipe.marins[j]) && nbre < nbMarin)
	     {
	      nbre++;
	      if(!equipe1.addMarin(equipe.marins[j]))
	       {
		    ok = false;
	       }
	     }
	    }
	    if(ok==true)
	    {
	     this.marins = equipe1.marins;
	    }

	   }
	   //ajouter � l'�quipage le nombre de places disponibles
	   public void etendEquipage(int entier)
	   {
	    Marin [] tabEquipe = new Marin[this.n + entier];
	    System.arraycopy(this.marins, 0, tabEquipe, 0, this.n);
	    this.marins = tabEquipe;
	    this.n = this.n + entier;
	   }


	  

	   @Override
	   public int hashCode() 
	   {		
	    int [] hashcodes = new int [this.getNombreMarins()];
	    int j = 0;
	    for (int i = 0; i < marins.length ; i++) 
	    {
	     if (marins[i]!=null) 
	     {
	      hashcodes[j]=marins[i].hashCode();
	      j++;
	     }
	    }
	    Arrays.sort(hashcodes);

	    final int prime = 31;
	    int result = 1;
	    result = prime * result + Arrays.hashCode(hashcodes);
	    result = prime * result + n;
	    return result;
	   }

	   @Override
	   public boolean equals(Object obj) 
	   {
	    if (this == obj)
	    return true;
	    if (obj == null)
	    return false;
	    if (getClass() != obj.getClass())
	    return false;
	    Equipage other = (Equipage) obj;
	    //if (!Arrays.equals(marins, other.marins))
	    //return false;
	    if(this.getNombreMarins() != other.getNombreMarins())
	    {
	      return false;
	    }
	    for(int i=0;i<other.n;i++)
	    {
	     if(other.marins[i]!=null && !other.isMarinPresent(this.marins[i]))
	     {
	      return false;
	     }
	    }
	    if (n != other.n)
		return false;

		return true;
	   }
		
	
}

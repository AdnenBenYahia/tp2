package serie3;

public class BatRames extends Bateau{

	TypeBateau type;
	
	
	public BatRames(String nom, int poids, Capitaine capitaine){
		super(nom,poids,capitaine);
		type = TypeBateau.BateauARames;
	}
	
	public String getTypeBateau()
	{
		return type.name();
	}
}

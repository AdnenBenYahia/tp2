package serie3;

public class BatVoiles extends Bateau{

	TypeBateau type;
	
	
	public BatVoiles(String nom, int poids, Capitaine capitaine){
		super(nom,poids,capitaine);
		type = TypeBateau.BateurAvoiles;
	}
	
	// Il faut declarer cette m�thode dans Bateau de type abstract
	public String getTypeBateau()
	{
		return type.name();
	}
}

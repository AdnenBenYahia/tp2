package serie3;

import serie3.Marin;

//import serie1.Marin;
public class Capitaine extends Marin{
	protected Grade grade;
	public enum Grade 
	{
		BOSCO,CAPITAINE
	}

	/*public Capitaine()
	{
	}*/

	public Capitaine(String nom, String prenom,int salaire,String mongrade){
		super(nom,prenom,salaire);
		grade = Grade.valueOf(mongrade);
	}
	@Override
	public String toString() {
		return "Capitaine [" + /*super.toString()+*/" grade= "+grade + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((grade == null) ? 0 : grade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Capitaine other = (Capitaine) obj;
		if (grade != other.grade)
			return false;
		return true;
	}

	public Grade getCapitaine() {
		return grade;
	}
    

	
}

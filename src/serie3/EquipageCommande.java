package serie3;
import serie3.Equipage;
public class EquipageCommande extends Equipage 
{
	private Capitaine commandant;


	public EquipageCommande(Capitaine capitaine)
	{
		super(5);
		this.commandant = capitaine;	
	}

	
	public Capitaine getCapitaine() {
		return commandant;
	}

	public void setCapitaine(Capitaine capitaine) {
		this.commandant = capitaine;
	}

	@Override
	public String toString() {
		return  commandant + " " + super.toString();
	}
	
}
